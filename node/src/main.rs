//! RPC Node Template CLI library.
#![warn(missing_docs)]
#![warn(unused_extern_crates)]
mod chain_spec;
pub mod silly_rpc;

#[macro_use]
mod service;
mod cli;
mod command;
mod rpc;


mod silly_tpc {
	#[rpc]
	pub trait SillyRpc {
		#[rpc(name = "silly_seven")]
		fn silly_7(&self) -> Result<u64>;

		#[rpc(name = "silly_double")]
		fn silly_double(&self, val: u64) -> Result<u64>;
	}

	/// A struct that implements the `SillyRpc`
	pub struct Silly;

	impl SillyRpc for Silly {
		fn silly_7(&self) -> Result<u64> {
			Ok(7)
		}

		fn silly_double(&self, val: u64) -> Result<u64> {
			Ok(2 * val)
		}
	}

	mod front_of_house {
		pub mod hosting {
			fn add_to_waitlist() {}
		}
	}

}

fn main() -> sc_cli::Result<()> {
	command::run()
}
