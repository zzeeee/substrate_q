
				use substrate_wasm_builder::build_project_with_default_rustflags;

				fn main() {
					build_project_with_default_rustflags(
						"/home/andrey/substrate-node-template/target/debug/build/api-runtime-70316b0975c192d9/out/wasm_binary.rs",
						"/home/andrey/runtimes/api-runtime/Cargo.toml",
						"-Clink-arg=--export=__heap_base -C link-arg=--import-memory ",
					)
				}
			