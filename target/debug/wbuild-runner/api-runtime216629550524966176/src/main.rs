
				use substrate_wasm_builder::build_project_with_default_rustflags;

				fn main() {
					build_project_with_default_rustflags(
						"/home/andrey/substrate-node-template/target/debug/build/api-runtime-8ea4a465e61669ab/out/wasm_binary.rs",
						"/home/andrey/substrate-node-template/runtimes/api-runtime/Cargo.toml",
						"-Clink-arg=--export=__heap_base -C link-arg=--import-memory ",
					)
				}
			