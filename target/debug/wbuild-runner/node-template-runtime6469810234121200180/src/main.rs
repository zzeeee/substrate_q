
				use substrate_wasm_builder::build_project_with_default_rustflags;

				fn main() {
					build_project_with_default_rustflags(
						"/home/andrey/substrate-node-template/target/debug/build/node-template-runtime-68233fff75f38f08/out/wasm_binary.rs",
						"/home/andrey/substrate-node-template/runtime/Cargo.toml",
						"-Clink-arg=--export=__heap_base -C link-arg=--import-memory ",
					)
				}
			